angular.module('app', [])
 .controller('ctrl', function ($scope) {
   $scope.time = '5:00 pm';
   $scope.result = function () {
     var now = new Date();
     var nowTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours()+":"+now.getMinutes());
     var userTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + $scope.time);
     if (nowTime.getTime() > userTime.getTime()) {
       return "Closed Now !!";
     }else {
       return "Open Now !!";
     }
   }
 })